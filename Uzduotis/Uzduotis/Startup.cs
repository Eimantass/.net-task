﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Uzduotis.Startup))]
namespace Uzduotis
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
