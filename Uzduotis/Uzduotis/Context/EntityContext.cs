﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Uzduotis.Models;

namespace Uzduotis.Context
{
    public class EntityContext : DbContext
    {
        public DbSet<User> User { get; set; }
        public DbSet<Tasks> Tasks { get; set; }
    }
}