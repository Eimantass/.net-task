﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Uzduotis.Models
{
    public class ViewModel
    {
        public User User { get; set; }
        public IEnumerable<Tasks> Tasks { get; set; }
    }
}