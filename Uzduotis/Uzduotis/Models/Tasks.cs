﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Uzduotis.Models
{
    public class Tasks
    {
        [Key]
        public int User_Id { get; set; }
        public string Name { get; set; }
        public bool Completed { get; set; }
    }
}